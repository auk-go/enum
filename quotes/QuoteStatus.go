package quotes

type QuoteStatus struct {
	IsQuoteFound bool
	Found        Quote
	IsLeft       bool
}
