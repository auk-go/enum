package linuxservicestate

import (
	"gitlab.com/auk-go/core/constants"
)

const (
	InvalidExitCode = constants.MinInt
)
