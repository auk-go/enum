module gitlab.com/auk-go/enum

go 1.17.8

require (
	github.com/smartystreets/goconvey v1.8.1
	gitlab.com/auk-go/core v1.4.4
	golang.org/x/sys v0.13.0
)

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smarty/assertions v1.15.1 // indirect
)
