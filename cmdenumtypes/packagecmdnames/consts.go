package packagecmdnames

const (
	CommandName         = "pkg"
	FullCommandName     = "package"
	ManyCommandName     = "pkgs"
	ManyFullCommandName = "packages"
)
