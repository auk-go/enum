package linuxtype

var isUbuntuMap = map[Variant]bool{
	UbuntuServer:   true,
	UbuntuServer18: true,
	UbuntuServer19: true,
	UbuntuServer20: true,
	UbuntuServer21: true,
	UbuntuServer22: true,
	UbuntuServer23: true,
	UbuntuDesktop:  true,
}
