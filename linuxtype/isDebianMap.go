package linuxtype

var isDebianMap = map[Variant]bool{
	DebianServer:   true,
	DebianServer7:  true,
	DebianServer8:  true,
	DebianServer9:  true,
	DebianServer10: true,
	DebianServer11: true,
	DebianServer12: true,
	DebianServer13: true,
	DebianServer14: true,
	DebianDesktop:  true,
}
