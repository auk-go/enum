# Code Samples

```

var isDockerMap = map[Variant]bool{
	Unknown:              true,
	UbuntuServer:         true,
	UbuntuServer18:       true,
	UbuntuServer19:       true,
	UbuntuServer20:       true,
	UbuntuServer21:       true,
	UbuntuServer22:       true,
	UbuntuServer23:       true,
	Centos:               true,
	Centos7:              true,
	Centos8:              true,
	Centos9:              true,
	Centos10:             true,
	Centos11:             true,
	Centos12:             true,
	CentosStream:         true,
	DebianServer:         true,
	DebianServer7:        true,
	DebianServer8:        true,
	DebianServer9:        true,
	DebianServer10:       true,
	DebianServer11:       true,
	DebianServer12:       true,
	DebianServer13:       true,
	DebianServer14:       true,
	DebianDesktop:        true,
	Docker:               true,
	DockerUbuntuServer:   true,
	DockerUbuntuServer18: true,
	DockerUbuntuServer19: true,
	DockerUbuntuServer20: true,
	DockerUbuntuServer21: true,
	DockerUbuntuServer22: true,
	DockerCentos7:        true,
	DockerCentos8:        true,
	DockerCentos9:        true,
	DockerCentos10:       true,
	Android:              true,
	UbuntuDesktop:        true,
}


```
